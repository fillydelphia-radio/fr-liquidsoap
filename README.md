# FillyRadio Liquidsoap Script

The script used to power Fillydelphia Radio's audio streams


## Important stuff

We use Liquidsoap 1.3.6
This script may not work on older, or newer, versions.

**You can use https://gitlab.com/fillydelphia-radio/liquidsoap-docker instead of building all this!**


## Brief Instructions to build LS for this script:

Install `opam` from your package manager. Switch to the user you want to run liquidsoap under.

*Opam, at this time, does not install these packages globally, so you need to run these scripts as the user you build them under.*

*See https://github.com/ocaml/opam/issues/1950 regarding this issue*

To specifically run this script, you need these packages installed via Opam: 


* cry
* mad
* vorbis
* ogg
* faad
* fdkaac
* flac
* inotify
* lame
* lo
* magic
* opus
* samplerate
* soundtouch
* speex
* ssl
* taglib
* theora
* voaacenc
* xmlplaylist
* yojson
* liquidsoap


Run `opam init`, and add the suggestions to `~/.bashrc`

Check what version of Ocaml you are running:

`ocaml --version`

You need to be on `4.05.0`. `4.06.0`, as of the time of writing, refuse to compile.

If you have to, use `opam switch 4.05.0`

This will download and compile Ocaml `4.05.0`.

To calculate what you need, run `opam depext <packages>`:

`opam depext cry faad fdkaac flac inotify lame lo mad magic ogg opus samplerate soundtouch speex ssl taglib theora voaacenc vorbis xmlplaylist yojson liquidsoap`

Then, allow opam to use your system's package manager to install the missing dependencies.

Once done, run:

`opam depext cry faad fdkaac flac inotify lame lo mad magic ogg opus samplerate soundtouch speex ssl taglib theora voaacenc vorbis xmlplaylist yojson liquidsoap`


## The script itself:


### Definitions


#### Metadata to Firebase

The very first def, send_meta instructs Liquidsoap to run `curl`, which will take the metadata fed to it later in the script, and make a `PUT` request to Firebase Realtime Database. This allows sites and apps to get metadata sent to them as soon as the server starts playing the track. If you don't need this, remove this section, and the line further down that says 

`staudio = on_metadata(send_meta,staudio)`

Otherwise, if you do want it, make an account at https://console.firebase.com/, create a project, and enable the Firebase Realtim Database. Replace the URL to the JSON endpoint with the one you create (`json_of(m)) ^ "' 'https://fillydelphia-radio.firebaseio.com/rest/fillydelphia-radio/now-playing.json?auth='"`). You will need to generate an auth token. This requires the deprecated legacy key auth, called a "Server Key", which you can get from Project Settings => Cloud Messaging => Server Key in your Firebase Console. Add that to the end, after `auth=`


#### Silence Detection and Log

This is a logging feature, that detects if the stream has become faulty, if staudio plays silence for more than the time specified by max_blank lower down, as shown here:

```
staudio = on_blank(
    max_blank=7.5,
    silence,
    staudio
)
```

#### Crossfades

If you know what you are doing, you can tweak these. I recommend you read up the documentation first: http://savonet.sourceforge.net/doc-svn/reference.html

### Variables

#### Set up main server variables

These all set up Liquidsoap before it starts to operate. These variable overwrite built-ins, in order to make things run smoother.

`set("log.file.path","/tmp/ls-fr.log")` Outputs the logfile  
`set("log.stdout",true)` Logs to stdout  
`set("log.level",4)` VERY VERY verbose  
`set("decoder.file_extensions.taglib",["mp3"])` Makes sure taglib reads MP3 files, which fixes some bugs  
`set("decoder.mime_types.taglib",["audio/mpeg"])` Does the same as above  
`set("encoder.encoder.export",["artist","title","album"])` Makes sure that the encoder pushes artist, title and album  
`set("tag.encodings",["UTF-8"])` Makes sure no funky UTF-16 characters cause issues.  

#### Live DJ Setup

`set("harbor.verbose",true)` Print harbor connections to log  
`set("harbor.bind_addr","0.0.0.0")` Listen on all interfaces  
`set("harbor.timeout",15.0)` If a source connects, but fails to send data, the server will sever the connection after 15 seconds

### Harbour Input

Pretty simple, this part defines the icecast harbour, so DJs can connect and broadcast "over the top" of the autodj

```
live = input.harbor(
            "stream",
            icy=true,
            buffer=5.,
            max=10.0,
            port=8080,
            password=""
        )

```
`output.dummy(fallible=true,live)` allows the source to fail gracefully

### Playlist Setup

This part of the script defines the playback for the AutoDJ.

```
autodj = smart_crossfade(
                playlist(
                    conservative=true,
                    mode='randomize',
                    reload=86400,
                    reload_mode="seconds",
                    "/home/liquidsoap/radio/audio/music"
                )
            )
jingles = playlist("/home/liquidsoap/radio/audio/id")
offline = rotate(weights=[1,3],[ jingles, autodj ])
```

`smart_crossfade` tells the AutoDJ to use the smart_crossfade we defined earlier.
`playlist tells the` AutoDJ to treat this as a long playlist
`conservative=true` tells the playlist to not count the remaining time in a song as part of the playlist length
`mode=randomization` tells the playlist to use a random number generator to decide what plays next
`reload=86400` tells the playlist to dump itself every 86400 seconds (24 hours), and reload the playlist
`reload_mode="seconds"` tells the reload method to use seconds, as opposed to "rounds", which is a liquidsoap measurement, or "watch", which watches a file for changes (doesn't work here, as we aren't loading a playlist file).
`"/home/liquidsoap/radio/audio/music"` this tells playlist what we want to load, a folder of MP3 files. Playlist is smart enough to treat a directory as a playlist.
`jingles = playlist("/home/liquidsoap/radio/audio/id")` is similar to the block above, but seeing as we only need to play one jingle every 3 songs, and we don't seem to add any new ones, we can leave this at it's defaults.
`offline = rotate(weights=[1,3],[ jingles, autodj ])` This is what makes sure our promos play, once every three songs.


### Fallbacks

These dictate the fallback nature of the stream. First priority is broadcasters, second is `autodj`.

First, we want AutoDJ to fall back onto itself every 24 hours. This causes the AutoDJ to reload itself.
This might seem redundant, but it solves a garbage collection issue, which keeps the RAM usage nice and small.

```
staudio = fallback ([
                switch(
                    track_sensitive=true,
                    [
                        ({ (0w-6w) and 0h - 24h}, offline),
                        ({ true },autodj),
                    ]
                ),
                autodj
            ])
```
Then, we want to define that staudio should be live, that is, our output should be the `icecast.harbor`. Then, if that harbor is unavailable, that it should gracefully fall back to the autodj.

```
staudio = fallback(
                track_sensitive=false,
                transitions=[to_live,to_offline],
                # strip blank allows for 7.5 seconds of silence, before playing over the top.
                [strip_blank(max_blank=7.5,min_noise=0.,live),staudio]
          )
```

`[strip_blank(max_blank=7.5,min_noise=0.,live),staudio]` tells fallback to consider live as unavailable if there is silence on `live` for more than 7.5 seconds, and fall back to `autodj`.


### Add AGC settings:


> TODO: Replace this section with Stereo Tool via Jackd

```
staudio=normalize(gain_max=4.5,gain_min=-4.5,k_down=0.08,k_up=0.005,target=-9.,threshold=-40.,window=0.77,staudio)
```

This tool makes the playback a nice, even volume, and boosts quiet presenters. We used to use LADSPA plugins here, which sounded a bit better, but they're no longer available.


### Detect Silence

This little piece simply tells Liquidsoap to log silence as an error (check above in  definitions):

```
staudio = on_blank(
    max_blank=7.5,
    silence,
    staudio
)
```

### Outputs

These are configured to push to Icecast, but you could technically push anywhere, such as a file, Twitch (if you add some video somewhere), Shoutcast, you just gotta config it.

Replace the outputs to match your own setup. You can get some detailed instructions on the encoders here: http://savonet.sourceforge.net/doc-svn/encoding_formats.html

### Final Dummy

This bit prevents some overruns, and stops a lot of log messages.